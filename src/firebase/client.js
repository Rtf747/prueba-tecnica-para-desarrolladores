import { initializeApp } from 'firebase/app';
import { GithubAuthProvider, signInWithPopup, getAuth } from 'firebase/auth';
import {
 getFirestore,
 collection,
 query,
 where,
 getDocs,
} from 'firebase/firestore';

const firebaseConfig = {
 apiKey: 'AIzaSyBpCuiw7uoVt-ihJbqjUeyNEjqQwlFNs0k',
 authDomain: 'technical-test-6a9a9.firebaseapp.com',
 databaseURL: 'https://technical-test-6a9a9-default-rtdb.firebaseio.com',
 projectId: 'technical-test-6a9a9',
 storageBucket: 'technical-test-6a9a9.appspot.com',
 messagingSenderId: '1036772990378',
 appId: '1:1036772990378:web:ce2d05d90b0f8144f31fd3',
 measurementId: 'G-RTM3CW5RNX',
};

export const firebaseApp = initializeApp(firebaseConfig);
const database = getFirestore(firebaseApp);

export const firstQuery = async () => {
 let response = [];

 const databaseQuery = query(
  collection(database, 'houses'),
  where('yearOfConstruction', '>=', 2008)
 );

 const querySnapshot = await getDocs(databaseQuery);
 querySnapshot.forEach((document) => {
  response.push(document.id, ' => ', document.data());
 });

 const filterByType = response.filter((el) => typeof el === 'object');

 return filterByType;
};

export const loginWithGithub = async () => {
 const provider = new GithubAuthProvider();
 const auth = getAuth(firebaseApp);
 const response = await signInWithPopup(auth, provider);

 return response;
};
