import Checkbox from './Checkbox';
import './customDropDown.css';

const citys = ['Belo Horizonte', 'London', 'Naples', 'Oslo', 'Vancouver'];
const CityDropDown = ({ check, handleChange }) => {
 return (
  <>
   <div className='dropdown'>
    <button className='dropbtn'>By city</button>
    <div className='dropdown-content'>
     {citys.map((city) => (
      <Checkbox
       type='checkbox'
       name={city}
       className={'city'}
       checked={check.city}
       onChange={handleChange}
       label={city}
       key={city}
      />
     ))}
    </div>
   </div>
  </>
 );
};

export default CityDropDown;
