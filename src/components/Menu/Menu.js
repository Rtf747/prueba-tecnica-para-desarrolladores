import styled from 'styled-components';
import YearDropDown from './YearDropMenu';
import './customDropDown.css';
import CityDropDown from './CityDropDown';
import StatusDropDown from './StatusDropDown';

const Aside = styled.aside`
 z-index: 999;
 position: fixed;
 background-color: gray;
 width: 30%;

 transition: all 0.5s;
 transform: ${({ showMenu }) =>
  showMenu ? 'translateX(0)' : 'translateX(-110%)'};
`;

const Container = styled.section`
 display: flex;
 flex-direction: column;
`;

const Menu = ({ showMenu, handleChange, check }) => {
 return (
  <>
   <Aside showMenu={showMenu}>
    <Container>
     <h2>Filter your search</h2>
     <YearDropDown handleChange={handleChange} check={check} />
     <CityDropDown handleChange={handleChange} check={check} />
     <StatusDropDown handleChange={handleChange} check={check} />
    </Container>
   </Aside>
  </>
 );
};

export default Menu;
