const Checkbox = ({ type, name, checked, onChange, label, className }) => {
 return (
  <>
   <label>
    <input
     type={type}
     name={name}
     checked={checked}
     onChange={onChange}
     className={className}
    />
    {label}
   </label>
  </>
 );
};

export default Checkbox;
