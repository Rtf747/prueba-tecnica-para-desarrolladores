import Checkbox from './Checkbox';
import './customDropDown.css';

const years = [2010, 2011];
const YearDropDown = ({ check, handleChange }) => {
 return (
  <>
   <div className='dropdown'>
    <button className='dropbtn'>By year of construction</button>
    <div className='dropdown-content'>
     {years.map((year) => (
      <Checkbox
       type='checkbox'
       name={year}
       className={'year'}
       checked={check.year}
       onChange={handleChange}
       label={year}
       key={year}
      />
     ))}
    </div>
   </div>
  </>
 );
};

export default YearDropDown;
