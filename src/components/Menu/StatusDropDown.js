import Checkbox from './Checkbox';
import './customDropDown.css';

const Status = ['Presale', 'On sale', 'Sold out'];
const CityDropDown = ({ check, handleChange }) => {
 return (
  <>
   <div className='dropdown'>
    <button className='dropbtn'>By status</button>
    <div className='dropdown-content'>
     {Status.map((state) => (
      <Checkbox
       type='checkbox'
       name={state}
       className={'status'}
       checked={check.state}
       onChange={handleChange}
       label={state}
       key={state}
      />
     ))}
    </div>
   </div>
  </>
 );
};

export default CityDropDown;
