import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
 box-shadow: 0 5px 2px rgba(0, 0, 0, 0.5);
`;

const TextContainer = styled.div`
 font-size: medium;
 color: black;
 margin: 1rem;
 margin-bottom: 0;
`;

const Image = styled.img`
 width: 100%;
 height: 50%;
 margin: ;
`;
const NumberFormat = new Intl.NumberFormat('en-US');

const Card = ({ data }) => {
 return (
  <Container>
   <Image src='https://picsum.photos/400/400' alt='' />
   <TextContainer>
    <p>{data.status}</p>
    <p>{`$ ${NumberFormat.format(data.price)}`}</p>
    <p>{data.description}</p>
    <p>{data.city}</p>
    <p>{data.address}</p>
    <p>{data.yearOfConstruction}</p>
   </TextContainer>
  </Container>
 );
};

export default Card;
