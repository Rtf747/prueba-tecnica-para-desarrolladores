import styled from 'styled-components';
import StateHandler from './StateHandler';
import { useState } from 'react';
import { useEffect } from 'react';
import { firstQuery } from '../../firebase/Client';

const Grid = styled.div`
 display: grid;
 grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
 grid-auto-rows: 400px;
 grid-gap: 1rem;
 margin: 3rem 4rem;
`;

const SkeletonNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];

const CardGrid = () => {
 const [data, setData] = useState(null);
 const [state, setState] = useState({
  loading: true,
 });

 useEffect(() => {
  const fetchHouses = async () => {
   const response = await firstQuery();
   setData(response);
   return response;
  };
  fetchHouses().catch(console.error);
 }, []);

 useEffect(() => {
  if (data) {
   setState({ ...state, loading: false });
  } else {
   setState({ ...state, loading: true });
  }
 }, [data]);

 return (
  <>
   <Grid className='sc-crHmcD'>
    <StateHandler
     state={state}
     data={data}
     loading={state.loading}
     SkeletonNumbers={SkeletonNumbers}
    />
   </Grid>
  </>
 );
};

export default CardGrid;
