import SkeletonArticle from '../../skeletons/SkeletonArticle';

const LoadingComponent = ({ loading, SkeletonNumbers }) => {
 if (loading) {
  return (
   <>
    {SkeletonNumbers.map((el) => (
     <SkeletonArticle key={el} />
    ))}
   </>
  );
 }
};

export default LoadingComponent;
