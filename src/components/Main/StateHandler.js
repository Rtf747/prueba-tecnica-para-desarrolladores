import LoadingComponent from './LoadingComponent';
import Card from './Card';

const StateHandler = ({ data, loading, SkeletonNumbers }) => {
 if (loading) {
  return (
   <>
    <LoadingComponent loading={loading} SkeletonNumbers={SkeletonNumbers} />
   </>
  );
 }

 if (data) {
  if (data.length === 0) {
   return (
    <>
     <p>No Data!</p>
    </>
   );
  }

  if (data.length > 0) {
   return (
    <>
     {data.map((el, index) => (
      <Card data={el} key={index} />
     ))}
    </>
   );
  }
 }
};

export default StateHandler;
