import MenuIcons from '@material-ui/icons/Menu';

const MenuIcon = ({ showMenu, setShowMenu }) => {
 const handleClick = (e) => {
  !showMenu ? setShowMenu(true) : setShowMenu(false);
 };

 return (
  <>
   <MenuIcons className='iconMenu' onClick={handleClick} />
  </>
 );
};

export default MenuIcon;
