import SignInContext from '../../context/SignInContext';
import { useContext } from 'react';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ProfilePicture from '../Header/ProfilePicture';
import SkeletonProfile from '../../skeletons/SkeletonProfile';

const Account = () => {
 const { user, signIn, logOut } = useContext(SignInContext);

 return (
  <>
   {user === undefined && <SkeletonProfile />}
   {user === null && <AccountCircle className='iconProfile' onClick={signIn} />}
   {user && user && <ProfilePicture logOut={logOut} userInfo={user} />}
  </>
 );
};

export default Account;
