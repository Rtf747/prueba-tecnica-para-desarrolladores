import styled from 'styled-components';

const Image = styled.img`
 border-radius: 50%;
 width: 3.25rem;
 height: 3.25rem;
 margin-right: 2rem;
 cursor: pointer;
 margin: 10px 0;
 margin-right: 2rem;
`;

const ProfilePicture = ({ userInfo, logOut }) => {
 return (
  <>
   <Image src={userInfo.photoURL} onClick={logOut} alt='' />
  </>
 );
};

export default ProfilePicture;
