import styled from 'styled-components';
import Account from './Account';
import Title from './Title';
import MenuIcon from './MenuIcon';

const Container = styled.section`
 display: flex;
 align-items: center;
 justify-content: space-between;
 border: 2px solid white;
 width: 100%;
`;

const Header = ({ showMenu, setShowMenu }) => {
 return (
  <Container>
   <MenuIcon showMenu={showMenu} setShowMenu={setShowMenu} />
   <Title />
   <Account />
  </Container>
 );
};

export default Header;
