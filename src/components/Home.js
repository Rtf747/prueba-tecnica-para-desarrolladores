import { useState } from 'react';
import Header from './Header/Header';
import Main from './Main/Main';
import Menu from './Menu/Menu';

const initialCheck = {
 2010: false,
 2011: false,
 BeloHorizonte: false,
 Naples: false,
 London: false,
 Oslo: false,
 Vancouver: false,
 Presale: false,
 Onsale: false,
 Soldout: false,
};

const Home = () => {
 const [showMenu, setShowMenu] = useState(false);
 const [check, setCheck] = useState(initialCheck);

 const handleChange = (e) => {
  setCheck({
   ...check,
   [e.target.name.split(' ').join('')]: e.target.checked,
  });
 };
 return (
  <>
   <Header showMenu={showMenu} setShowMenu={setShowMenu} />
   <Menu showMenu={showMenu} handleChange={handleChange} check={check} />
   <Main />
  </>
 );
};

export default Home;
