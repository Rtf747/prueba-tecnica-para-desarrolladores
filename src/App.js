import Home from '../src/components/Home';
import { createGlobalStyle } from 'styled-components';
import { SignInProvider } from '../src/context/SignInContext';
const GlobalStyle = createGlobalStyle`
* {
  margin: 0;
  padding: 0;
}

body{
 background-color: white;
 box-sizing: border-box;
 border: solid white 10px;
width: 100%;
height: 100%;
}

.iconMenu{
  transform: scale(1.8);
  margin: 1rem;
  margin-left: 2rem;
  margin-right: 2rem;
  cursor: pointer;
}

.iconProfile{
  transform: scale(2.56);
  margin-top: 1.5rem;
  margin-bottom: 1.5rem;
  margin-right: 3rem;
  cursor: pointer;
}
`;

function App() {
 return (
  <>
   <SignInProvider>
    <GlobalStyle />
    <Home />
   </SignInProvider>
  </>
 );
}

export default App;
