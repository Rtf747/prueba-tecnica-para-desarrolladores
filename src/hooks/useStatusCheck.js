import { useState } from 'react';

const initialCheck = {
 2010: false,
 2011: false,
 BeloHorizonte: false,
 Naples: false,
 London: false,
 Oslo: false,
 Vancouver: false,
 Presale: false,
 Onsale: false,
 Soldout: false,
};

export const useStatusCheck = () => {
 const [check, setCheck] = useState(initialCheck);

 const handleChange = (e) => {
  setCheck({
   ...check,
   [e.target.name.split(' ').join('')]: e.target.checked,
  });
 };

 return { check, handleChange, initialCheck };
};
