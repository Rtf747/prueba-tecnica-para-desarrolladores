import { createContext, useState } from 'react';
import { getAuth, signOut, onAuthStateChanged } from 'firebase/auth';
import { loginWithGithub } from '../firebase/Client';

const SignInContext = createContext();

const SignInProvider = ({ children }) => {
 const [user, setUser] = useState(undefined);

 const auth = getAuth();

 const signIn = async () => {
  const logIn = await loginWithGithub();
 };

 const logOut = async () => {
  const closeSession = await signOut(auth);
  console.log(closeSession);
 };

 onAuthStateChanged(auth, (user) => {
  setTimeout(() => {
   user ? setUser(user) : setUser(null);
  }, 2000);
 });

 const data = { user, signIn, logOut };

 return (
  <SignInContext.Provider value={data}>{children}</SignInContext.Provider>
 );
};

export { SignInProvider };
export default SignInContext;
