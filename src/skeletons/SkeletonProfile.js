import styled from 'styled-components';
import Shimmer from './Shimmer';
import SkeletonElement from './SkeletonElements';

const Container = styled.div`
 margin: 0;
 margin-right: 2rem;
 padding: 0;
`;

const SkeletonProfile = ({ theme }) => {
 const themeClass = theme || 'light';

 return (
  <Container className={`skeleton-wrapper ${themeClass}`}>
   <div className='skeleton-article'>
    <SkeletonElement type='avatar' />
   </div>
   <Shimmer />
  </Container>
 );
};

export default SkeletonProfile;
