# Esta es la solución de la Prueba técnica para desarrolladores

## Links

- [Solución URL](https://gitlab.com/Rtf747/prueba-tecnica-para-desarrolladores)
- [URL del sitio en vivo](https://rtf747.github.io/prueba-tecnica/)

## Construido con

- Semantic HTML5 markup
- CSS custom properties
- CSS Flexbox
- CSS Grid
- [Styled Components](https://styled-components.com/) - 💅 Libreria de React
- [React](https://reactjs.org/) - ⚛ Libreria de JS
- [Firebase Authentication](https://firebase.google.com/docs/auth)
- [Firebase Firestone Database](https://firebase.google.com/docs/firestore)

## Autor

- [Github](https://github.com/Rtf747)
- [@Rtf747](https://www.frontendmentor.io/profile/Rtf747)
